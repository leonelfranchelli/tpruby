require 'bundler'
Bundler.require
require 'data_mapper'
require 'sinatra'
require 'rubygems'

DataMapper.setup(:default,{
	:adapter  => 'sqlite3',
	:host     => 'localhost',
	:username => '',
	:password => '',
	:database => '/home/lothorien/Documents/TPfinal/DB/NavalBattle'
	})

#MODELOS
class Board 
	include DataMapper::Resource
	property :id, Serial
	property :size, Integer
  property :left, Integer

  belongs_to :game, :required => false
  belongs_to :player
  has n, :ships
  has n, :plays
end
class Game 
	include DataMapper::Resource
	property :id, Serial
	property :turn, Integer
	property :finished, Integer
	property :started, Integer
  
	has n, :boards
end
class Player 
	include DataMapper::Resource
	property :id, Serial
	property :user, String
	property :pass, String

	has n, :boards
end
class Play
	include DataMapper::Resource
	property :id, Serial
	property :x, Integer
	property :y, Integer

	belongs_to :board

end
class Ship
	include DataMapper::Resource
	property :id, Serial
	property :destroyed, Integer
	property :x, Integer
	property :y, Integer

	belongs_to :board
end


DataMapper.finalize
DataMapper.auto_migrate!
enable :sessions #Habilitamos las sesiones
 #Datos de prueba
 @player1 = Player.create(
 	:user => "marta",    
 	:pass => "1234"
 	)
 @player2 = Player.create(
 	:user => "juan",    
 	:pass => "1234"
 	)

 get "/" do
 	redirect "/login"
 end

 get "/login" do
 	erb :login
 end

 get "/logout" do
  session.clear
  redirect '/login'
 end

 post "/login" do
 	user = params[:user]
 	pass = params[:password]
 	player = Player.all(:user => "#{user}")
 	if !player.empty?
 		if player.first.pass == pass
 			session[:id] = player.first.id
 			session[:user] = user
 			session[:pass] = pass
 			redirect "/players/#{player.first.id}/games"
 		else
     		redirect "/bad-pass" #contraseña invalida TODO:: En realidad esto seria un mensaje con, ?javascript?
     	end
     else
    redirect "/bad-user" #usuario no encontrado
  end
end 

get "/players/:id/games" do #listado de partidas
	@session = session[:id]
	@games = Game.all(:finished => nil, :started => 2, :turn => @session)
	@to_start = Game.all(:finished => nil, :started.lt => 2, :turn => @session)
  @finished = Game.all(:finished.not => nil, :boards => {:player => { :id => session[:id] } })
	@players = Player.all(:id.not => @session) #los jugadores que no sea yo (en realidad deberian aparecer contra los que no tengo partidas activas)
	erb :playergames
end

put "/players/:id/games/:game_id" do
	@game = Game.all(:id => params[:game_id]).first

	@my_board = @game.boards.select { |board| board.player.id == session[:id] }
	checkboxes = params[:ships]

  case @my_board.first.size
    when 1
      if checkboxes.length != 7
        session[:error] = 'You should place 7 ships'
        return redirect "/players/#{session[:id]}/games/newgame/#{params[:game_id]}"
      end
    when 2
      if checkboxes.length != 15
        session[:error] = 'You should place 15 ships'
        return redirect "/players/#{session[:id]}/games/newgame/#{params[:game_id]}"
      end
    when 3
      if checkboxes.length != 20
        session[:error] = 'You should place 20 ships'
        return redirect "/players/#{session[:id]}/games/newgame/#{params[:game_id]}"
      end
  end

	checkboxes.each { |checkbox|
		@x = checkbox.split('x')[0]
		@y = checkbox.split('x')[1]
		@ship = @my_board.first.ships.create(
			:destroyed => 0,
			:x => @x,
			:y => @y
			)
	}
	@other_board = @game.boards.reject { |e| e == @my_board.first }
	@game.update(:turn => @other_board.first.player.id, :started => @game.started+1)
	@my_board.first.save!
  redirect "/players/#{session[:id]}/games"
end

post "/players/games" do #se crea el juego
	@player1 = Player.all(:id =>session[:id])
	@player2 = Player.all(:id =>params[:rivals])
  case @params[:size].to_i #para poder inicializar la cantidad de barcos de cada tablero
  when 1
    @ships = 7
  when 2
    @ships = 15
  when 3
    @ships = 20
  end
  @board1 = @player1.first.boards.create(
    :size => params[:size],
    :left => @ships
    )
  @board2 = @player2.first.boards.create(
    :size => params[:size],
    :left => @ships
    )
  @game = Game.create(
   :turn => params[:rivals],
   :finished => nil,
   :started => 0,
   )
  @game.boards << @board1
  @game.boards << @board2
  @game.save!
  redirect "/players/#{session[:id]}/games/newgame/#{@game.id}"
end

get "/players/:id/games/:game_id" do
  @game = Game.all(:id => params[:game_id])
  @enemy_board = @game.first.boards.select { |board| board.player.id != session[:id] }
  @plays = @enemy_board.first.plays
  @ships_left = @enemy_board.first.left
  @board_size = @enemy_board.first.size
  case @board_size
  when 1
    erb :playingboard5x5
  when 2
    erb :playingboard10x10
  when 3
    erb :playingboard15x15
  end
end

post "/players/:id/games/:game_id/move" do
  @game = Game.all(:id => params[:game_id])

  if not @game.first.finished.nil?
    session[:error] = 'YOU LOOOSE'
    return redirect "/players/#{session[:id]}/games"
  end

  @enemy_board = @game.first.boards.select { |board| board.player.id != session[:id] }
  @x = params[:attack].split('x')[0].to_i
  @y = params[:attack].split('x')[1].to_i
  # TODO: Chequear que ese X e Y no esten en otra jugada
  plays = @enemy_board.first.plays.select { |play| play.x == @x && play.y == @y  }

  if(not plays.empty?)
    session[:error] = 'A play in the same position already exists'
    p session[:error]
    return redirect "/players/#{session[:id]}/games/#{@game.first.id}" 
  end

  @play = @enemy_board.first.plays.create(
    :x => @x,
    :y => @y
    )

  take_down = @enemy_board.first.ships.detect { |ship| ship.x == @x && ship.y == @y }

  if(take_down)
    left = @enemy_board.first.left-1
    @enemy_board.first.update(:left => left)

    if left.zero?
      session[:error] = 'YOU WON DUDE, EPIC !'
      @game.first.update(:finished => session[:id])
    else
      session[:error] = 'You took down an enemy ship, CONGRATS PIRATE ARRGH'
    end
    
    take_down.update(:destroyed => 1)
  end

  @enemy_board.first.save!
  @game.update(:turn => @enemy_board.first.player.id)
  redirect "/players/#{session[:id]}/games"
end

get "/players/:id/games/newgame/:game_id" do
	@game = Game.all(:id => params[:game_id])
	@board_size = @game.first.boards.first.size
	case @board_size
	when 1
		erb :board5x5
	when 2
		erb :board10x10
	when 3
		erb :board15x15
	end
end

get "/players" do
	@players = Player.all
	status 200
	erb :players
end

post "/players" do 
  #que seria un nombre de usuario invalido?
  user = params[:user]
  pass = params[:password]
  player = Player.all(:user => "#{user}")
  if player.empty?
  	new_player = Player.create(
  		:user => user,
  		:pass => pass
  		)
  	session[:id] = new_player.id
  	session[:user] = user
  	session[:pass] = pass
  	redirect "/players/#{new_player.id}/games"
  	status 201 
  else
  	status 400
  end
end

get "/new-player" do
	erb :newplayer
end

get "/juego" do
	erb :board
end