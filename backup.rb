class Board 
	include DataMapper::Resource
	property :id, Serial
	property :id_player, Integer
	property :size, Integer
end
class Game 
	include DataMapper::Resource
	property :id, Serial
	property :id_board1, Integer
	property :id_board2, Integer
	property :turn, Integer
	property :finished, Integer
	property :started, Integer
end
class Player 
	include DataMapper::Resource
	property :id, Serial
	property :user, String
	property :pass, String
end
class Play
	include DataMapper::Resource
	property :id, Serial
	property :id_board, Integer
	property :x, Integer
	property :y, Integer
	property :id_player2, Integer
end
class Ship
	include DataMapper::Resource
	property :id, Serial
	property :destroyed, Integer
	property :id_board, Integer
	property :x, Integer
	property :y, Integer
end


DataMapper.finalize
DataMapper.auto_migrate!
enable :sessions #Habilitamos las sesiones
 #Datos de prueba
 @player = Player.create(
 	:user => "marta",    
 	:pass => "1234"
 	)
 @player = Player.create(
 	:user => "juan",    
 	:pass => "12342"
 	)

 get "/" do
 	redirect "/login"
 end

 get "/login" do
 	erb :login
 end

 post "/login" do
 	user = params[:user]
 	pass = params[:password]
 	player = Player.all(:user => "#{user}")
 	if !player.empty?
 		if player.first.pass == pass
 			session[:id] = player.first.id
 			session[:user] = user
 			session[:pass] = pass
 			redirect "/players/#{player.first.id}/games"
 		else
      redirect "/bad-pass" #contraseña invalida NOTE: En realidad esto seria un mensaje con, ?javascript?
    end
  else
    redirect "/bad-user" #usuario no encontrado
  end
end 

get "/players/:id/games" do #listado de partidas
	@games = Game.all(:finished => 0, :started => 1) #TODO: hay que traer solo las del jugador
	@to_start = Game.all(:started => 0)
	@session = session[:id]
	@players = Player.all(:id.not => @session) #los jugadores que no sea yo (en realidad deberian aparecer contra los que no jugue)
	erb :playergames
end

put "/players/:id/games/:id game" do

end

post "/players/games" do #se crea el juego
	@board1 = Board.create(
		:id_player => session[:id],
		:size => params[:size]
		)
	@board2 = Board.create(
		:id_player => params[:rivals],
		:size => params[:size]
		)
	@game = Game.create(
		:id_board1 => @board1.id,
		:id_board2 => @board2.id,
    	:turn => @board2.id_player, #el jugador 2, deberia preguntar si tiene juegos asignados,
    	:finished => 0, # donde started sea 0, lo que significa que tiene que poner barcos en esos juegos
    	:started => 0
    	)
 redirect "/players/#{session[:id]}/games/#{@game.id}"
end

get "/players/:id/games/:game_id" do
	@game = Game.all(:id => params[:game_id])
	@board = Board.all(:id => @game.first.id_board1)
	case @board.first.size
	when 1
		erb :board5x5 #No funciona el puto PUT, no se como pija hacerlo AMEN
	when 2
		erb :board10x10
	when 3
    erb :board15x15
  end
end

get "/players" do
	@players = Player.all
	status 200
	erb :players
end

post "/players" do 
  #que seria un nombre de usuario invalido?
  user = params[:user]
  pass = params[:password]
  player = Player.all(:user => "#{user}")
  if player.empty?
  	new_player = Player.create(
  		:user => user,
  		:pass => pass
  		)
  	session[:id] = new_player.id
  	session[:user] = user
  	session[:pass] = pass
  	redirect "/players/#{new_player.id}/games"
  	status 201 
  else
  	status 400
  end
end

get "/new-player" do
	erb :newplayer
end

get "/juego" do
	erb :board
end
=begin
post "/juego" do
	Board.create(
	:id_player, Integer
	:size, Integer
	checkboxes = params[:column]
	checkboxes.each { |checkbox|
		Ship.create(
			:destroyed => 0,
			:id_board, Integer
			:x, Integer
			:y, Integer
end
			) }
	end
=end