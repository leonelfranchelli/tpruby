require 'minitest/autorun'
require 'minitest/spec'
include Rack::Test::Methods

require_relative 'app.rb'

describe 'testing /' do
	it 'should succed' do
		get '/'
		last_response.status.must_equal 200
		last_response.must_be :ok?
	end
end

describe 'login' do
	it 'should start a session with a user' do
		post '/login'
		session[:id].wont_be_nil
	end
	it 'should return 200'
	post '/login'
	last_response.status.must_equal 200
	last_response.must_be :ok?
end
end

describe 'testing /players' do
	#habria que crear un par de players
	it 'should create a new player' do
		post '/players'
		last_response.status.must_equal 201
	end
	it 'should be a conflit' do
		#algo que genere conflicto :)
last_response.status.must_equal 409
end
end
it 'should be a bad request' do
		#algo que sea un bad request 
		last_response.status.must_equal 400
	end
	it 'should return all the players'
	get '/players'
		#probar que esten los players creados arriba
	end
end

describe 'testing /players/id/games' do
	it 'should create a game' do 
		post '/players/:id/games'
		last_response.status.must_equal 409
	end
	it 'should fail to create a game' do
		#algo que falle "error de codificacion segun el enunciado"
		post '/players/:id/games'
		last_response.status.must_equal 400
	end
end

describe 'testing /players/:id/games/:id_game' do
	it 'should be able to show the game' do
		get '/players/:id/game/:id_game'
		last_response.status.must_equal 200
		last_response.must_be :ok?
	end
end

describe 'testing /players/:id/games' do
	it 'should be able to show all the games' do
		get '/players/:id/games'
		last_response.status.must_equal 200
		last_response.must_be :ok?
	end
end

describe 'testing /players/:id/games/:id_game' do
	it 'should add all the ships selected' do
		#esto hay que hacerlo?
	end
	it 'should success' do
		put '/players/:id/games/:id_game'
		last_response.status.must_equal 200
		last_response.must_be :ok?
	end
end

describe 'testing /players/:id/games/:id_game/move' do
	it 'should create a play' do
		post '/players/:id/games/:id_game/move'
		last_response.status.must_equal 201
	end
	it 'should not let the player move' do
		#agarrar un juego que no sea del jugador e intentar acceder al link
		last_response.status.must_equal 403
	end
	


